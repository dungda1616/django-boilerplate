const autoprefixer = require('autoprefixer');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');
const url = require('url');
const webpack = require('webpack');
const BundleTracker = require('webpack-bundle-tracker');
const SentryWebpackPlugin = require('@sentry/webpack-plugin');
const toBoolean = require('to-boolean');
const fs = require('fs');


const resolve = path.resolve.bind(path, __dirname);
require('dotenv').config({ path: resolve('./.env') });

const bundleConfigFile = 'webpack-stats.json';
const bundleConfigFileTemp = `pre-${bundleConfigFile}`;

if (!process.env.DEPLOY_ENV) process.env.DEPLOY_ENV = 'local';

// Use pre-webpack-bundle.json file for staging and production deploy
const bundleFileName = (process.env.DEPLOY_ENV == 'local' ) ?  bundleConfigFile : bundleConfigFileTemp;
const bundleTrackerPlugin = new BundleTracker({
  filename: `${bundleFileName}`,
});

const providePlugin = new webpack.ProvidePlugin({
});

module.exports = (_, argv) => {
  const subVersion = process.env.GIT_VERSION ? `${process.env.GIT_VERSION}/` : '';
  const publicStaticDomain = (process.env.STATIC_URL ? process.env.STATIC_URL.replace(/\/$/g, '') : `/static`) + '/';
  const devMode = argv.mode !== 'production';
  const output_path = resolve(`./static/dist/` + subVersion);
  const baseStaticPath = publicStaticDomain + subVersion;
  const publicPath = url.resolve(baseStaticPath, '');

  const output = {
    path: output_path,
    filename: '[name].js',
    publicPath: publicPath,
  };
  const fileLoaderPath = 'file-loader?name=[name].[ext]';
  const extractCssPlugin = new MiniCssExtractPlugin({
    filename: '[name].css',
  });

  plugins = [
    new webpack.DefinePlugin({
      'ENV_DEPLOY_ENV': JSON.stringify(process.env.DEPLOY_ENV),
    }),
    bundleTrackerPlugin,
    extractCssPlugin,
    providePlugin,
  ];

  if (process.env.SENTRY_DSN) {
    plugins.push(new SentryWebpackPlugin({
        include: output_path,
        ignore: ['node_modules', 'webpack.config.js'],
        release: `${process.env.GIT_VERSION}`,
        rewrite: true,
        // debug: true,
        // dryRun: true,
        urlPrefix: baseStaticPath,
        prefix: `/static/assets/${subVersion}/`,
    }));
  }

  plugins.push(new webpack.SourceMapDevToolPlugin({
    filename: '[name].js.map',
    // // Append this txt on end of the minified js files
    // // So next time sentry will automattic fetched from this
    // append: `\n//# sourceMappingURL=[url]`,
    publicPath: publicPath,
  }));

  const IS_DEBUG = toBoolean(process.env.DEBUG);
  return {
    entry: {
      app_main: resolve('./static/assets/app.js'),
    },
    output: output,
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          loader: 'babel-loader'
        },
        {
          test: /\.scss$/,
          use: [
            // --> Build new css output file
            // {
            //   loader: MiniCssExtractPlugin.loader,
            //   options: {
            //     publicPath,
            //   },
            // },

            // --> Build js + css in same ouput
            'style-loader',

            // Begin style loaders
            {
              loader: 'css-loader',
              options: {
                sourceMap: IS_DEBUG,
              }
            },
            {
              loader: 'postcss-loader',
              options: {
                sourceMap: IS_DEBUG,
                plugins: function() {
                  return [autoprefixer];
                }
              }
            },
            {
              loader: 'sass-loader',
              options: {
                sourceMap: IS_DEBUG
              }
            }
          ]
        },
        {
          test: /\.(eot|otf|png|svg|jpg|ttf|woff|woff2)(\?v=[0-9.]+)?$/,
          loader: fileLoaderPath,
          include: [
            resolve('node_modules'),
            resolve('static/fonts'),
            resolve('static/images'),
          ]
        },
      ]
    },
    optimization: {
      removeAvailableModules: false,
      removeEmptyChunks: false,
      splitChunks: false
    },
    plugins,
    devtool: false,
  };
};
