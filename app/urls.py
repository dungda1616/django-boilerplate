from app import settings
from django.conf.urls import include, url
from django.urls import path
from django.contrib import admin
from .core.urls import urlpatterns as core_urls


urlpatterns = [
    path('admin/', admin.site.urls),
    url(r"^", include(core_urls)),
]

if settings.DEBUG:
    from django.conf.urls.static import static
    from django.contrib.staticfiles.views import serve
    import debug_toolbar

    urlpatterns += [
        url(r"^__debug__/", include(debug_toolbar.urls)),
        # static files (images, css, javascript, etc.)
        url(r"^static/(?P<path>.*)$", serve)
    ] + static("/media/", document_root=settings.MEDIA_ROOT)
