import os.path
import django_cache_url
import dj_email_url
import dj_database_url
from django.utils.translation import gettext_lazy as _
from decouple import Config, RepositoryEnv
import calendar
import time


# Helper functions
#
# Convert string with "," delimiter as list
def _get_list(text):
    return [item.strip() for item in text.split(",")]


BASE_DIR = os.path.normpath(os.path.join(os.path.dirname(__file__), ".."))
DOTENV_FILE = os.path.join(BASE_DIR, '.env')
env_config = Config(RepositoryEnv(DOTENV_FILE))
SITE_ID = 1
DEBUG = env_config("DEBUG", default=False, cast=bool)
DEPLOY_ENV = env_config("DEPLOY_ENV", default='local')

WSGI_APPLICATION = "app.wsgi.application"

ALLOWED_CLIENT_HOSTS = _get_list(
    env_config("ALLOWED_CLIENT_HOSTS", default="*")
)
ALLOWED_HOSTS = _get_list(env_config("ALLOWED_HOSTS", default="localhost,127.0.0.1"))

INTERNAL_IPS = _get_list(env_config("INTERNAL_IPS", default="*"))

# Cache config
REDIS_URL = env_config("REDIS_URL", default='')
if REDIS_URL:
    CACHE_URL = os.environ.setdefault("CACHE_URL", REDIS_URL)
    CACHES = {
        "default": django_cache_url.config()
    }
else:
    if DEBUG:
        CACHES = {
            'default': {
                'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
            }
        }
    else:
        CACHES = {
            'default': {
                'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
                'LOCATION': os.path.join(BASE_DIR, "cache"),
                'TIMEOUT': 60,
                'OPTIONS': {
                    'MAX_ENTRIES': 1000
                }
            }
        }

# Database config
DATABASE_URL = env_config("DATABASE_URL", default="postgres://saleor:saleor@localhost:5432/saleor")
DATABASES = {
    "default": dj_database_url.config(
        default=DATABASE_URL, conn_max_age=600
    )
}

# General config
TIME_ZONE = 'Asia/Bangkok'
LANGUAGE_CODE = "en"
LANGUAGES = [
    ("en", _("English")),
    ("th", _("Thai")),
]
LOCALE_PATHS = (os.path.join(BASE_DIR, "locale"), )
USE_I18N = True
USE_L10N = True
USE_TZ = True
ROOT_URLCONF = "app.urls"

# Email config
EMAIL_URL = env_config("EMAIL_URL", default=None)
email_config = dj_email_url.parse(
    EMAIL_URL or "console://demo@example.com:console@example/"
)

EMAIL_FILE_PATH = email_config["EMAIL_FILE_PATH"]
EMAIL_HOST_USER = email_config["EMAIL_HOST_USER"]
EMAIL_HOST_PASSWORD = email_config["EMAIL_HOST_PASSWORD"]
EMAIL_HOST = email_config["EMAIL_HOST"]
EMAIL_PORT = email_config["EMAIL_PORT"]
EMAIL_BACKEND = email_config["EMAIL_BACKEND"]
EMAIL_USE_TLS = email_config["EMAIL_USE_TLS"]
EMAIL_USE_SSL = email_config["EMAIL_USE_SSL"]

ENABLE_SSL = env_config("ENABLE_SSL", default=False)
if ENABLE_SSL:
    SECURE_SSL_REDIRECT = not DEBUG

# Media & Static file config
MEDIA_ROOT = os.path.join(BASE_DIR, "media")
MEDIA_URL = env_config("MEDIA_URL", default="/media/")
STATIC_ROOT = os.path.join(BASE_DIR, "static")
STATIC_URL = env_config("STATIC_URL", default="/static/")
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static", "dist"),
]
STATICFILES_FINDERS = [
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
]


template_common_options = {
    "debug": DEBUG,
    "context_processors": [
        "django.contrib.auth.context_processors.auth",
        "django.template.context_processors.debug",
        "django.template.context_processors.i18n",
        "django.template.context_processors.media",
        "django.template.context_processors.static",
        "django.template.context_processors.tz",
        "django.contrib.messages.context_processors.messages",
        "django.template.context_processors.request",
    ],
    "loaders": [
        "django.template.loaders.filesystem.Loader",
        "django.template.loaders.app_directories.Loader",
    ],
    "string_if_invalid": '<< MISSING VARIABLE "%s" >>' if DEBUG else "",
}

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [
            os.path.join(BASE_DIR, "templates")
        ],
        "APP_DIRS": False,
        "OPTIONS": template_common_options,
    },
]

# Make this unique, and don't share it with anybody.
SECRET_KEY = env_config("SECRET_KEY", default="random_me")


MIDDLEWARE = [
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.security.SecurityMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    'django.middleware.locale.LocaleMiddleware',
]

# ============================= Revision ================================
# Get current timestamp as revision
# Run once time when start/deploy project
REVISION = calendar.timegm(time.gmtime())


INSTALLED_APPS = [
    # Django modules
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.sitemaps",
    "django.contrib.sites",
    "django.contrib.staticfiles",
    "django.contrib.auth",
    "django.contrib.postgres",
    "django.core.mail",
    "django.forms",
    "django.contrib.admin",
    # Extenal plugins
    "webpack_loader",
    # App modules
    "app.core",
]

ENABLE_DEBUG_TOOLBAR = env_config("ENABLE_DEBUG_TOOLBAR", default=False)
if ENABLE_DEBUG_TOOLBAR:
    # Ensure the debug toolbar is actually installed before adding it
    import debug_toolbar
    MIDDLEWARE = ("debug_toolbar.middleware.DebugToolbarMiddleware", *MIDDLEWARE)
    INSTALLED_APPS.append("debug_toolbar")

    DEBUG_TOOLBAR_PANELS = [
        # adds a request history to the debug toolbar
        "ddt_request_history.panels.request_history.RequestHistoryPanel",
        "debug_toolbar.panels.timer.TimerPanel",
        "debug_toolbar.panels.headers.HeadersPanel",
        "debug_toolbar.panels.request.RequestPanel",
        "debug_toolbar.panels.sql.SQLPanel",
        "debug_toolbar.panels.profiling.ProfilingPanel",
    ]

    # Force show debug toolbar
    def show_toolbar(request):
        return True

    DEBUG_TOOLBAR_CONFIG = {
        "RESULTS_CACHE_SIZE": 100,
        "INTERCEPT_REDIRECTS": env_config("ENABLE_INTERCEPT_REDIRECTS", default=False),
        "SHOW_TOOLBAR_CALLBACK": show_toolbar,
    }


LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "root": {"level": "INFO", "handlers": ["console"]},
    "formatters": {
        "verbose": {
            "format": (
                "%(levelname)s %(name)s %(message)s [PID:%(process)d:%(threadName)s]"
            )
        },
        "simple": {"format": "%(levelname)s %(message)s"},
    },
    "filters": {
        "require_debug_false": {"()": "django.utils.log.RequireDebugFalse"},
    },
    "handlers": {
        'errors_file': {
            'level': 'ERROR',
            'class': 'logging.handlers.RotatingFileHandler',
            # 5 MB
            'maxBytes': 1024*1024*5,
            'backupCount': 5,
            'filename': os.path.join(BASE_DIR, 'logs', 'logs_errors.log'),
            'filters': ['require_debug_false'],
        },
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "verbose",
        },
    },
    "loggers": {
        'django.request': {
            'handlers': ['errors_file'],
            'level': 'ERROR',
            'propagate': False,
        },
        "django": {
            "handlers": ["console", "errors_file"],
            "level": "INFO",
            "propagate": True,
        },
        "django.server": {"handlers": ["console"], "level": "INFO", "propagate": True},
        "saleor": {"handlers": ["console"], "level": "DEBUG", "propagate": True},
    },
}

# Webpack config

WEBPACK_LOADER = {
    "DEFAULT": {
        # "BUNDLE_DIR_NAME": os.path.join(BASE_DIR, "static", "dist"),
        # "STATS_FILE": os.path.join(BASE_DIR, "webpack-stats.json"),
        # "POLL_INTERVAL": 0.1,
        # 'IGNORE': [r'.+\.hot-update.js', r'.+\.map'],
        # 'LOADER_CLASS': 'app.ExternalWebpackLoader',

        'CACHE': not DEBUG,
        # must end with slash
        'BUNDLE_DIR_NAME': 'dist/',
        'STATS_FILE': os.path.join(BASE_DIR, 'webpack-stats.json'),
        'POLL_INTERVAL': 0.1,
        'TIMEOUT': None,
        'IGNORE': [r'.+\.hot-update.js', r'.+\.map'],
        'LOADER_CLASS': 'app.ExternalWebpackLoader',
    }
}


# CELERY SETTINGS
CELERY_BROKER_URL = (
    env_config("CELERY_BROKER_URL", default="")
)
CELERY_TASK_ALWAYS_EAGER = not CELERY_BROKER_URL
CELERY_ACCEPT_CONTENT = ["json"]
CELERY_TASK_SERIALIZER = "json"
CELERY_RESULT_SERIALIZER = "json"
CELERY_RESULT_BACKEND = env_config("CELERY_RESULT_BACKEND", default=None)
