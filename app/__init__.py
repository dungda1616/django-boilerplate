from .settings import DEPLOY_ENV

__all__ = ["celery_app"]
__version__ = DEPLOY_ENV

from webpack_loader.loader import WebpackLoader
import json


class ExternalWebpackLoader(WebpackLoader):
    def load_assets(self):
        stats_file = self.config['STATS_FILE']
        data = None
        with open(stats_file) as json_file:
            data = json.load(json_file)
        return data
