from django.shortcuts import render
from django.http import JsonResponse
from app.core.tasks import example_celery_task


def home(request):
    context = {'text_to_display': "Hello world!"}
    return render(request, 'index.html', context)


def trigger_celery(request):
    example_celery_task.delay('13')
    return JsonResponse({'status': 'done'}, status=200)


def handle_404(request, exception=None):
    return render(request, '404.html', {}, status=404)
