from app.celeryconf import app


@app.task
def example_celery_task(product_pk):
    print(product_pk)
