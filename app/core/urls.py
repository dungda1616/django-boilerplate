from app.core import views
from django.conf.urls import url

urlpatterns = [
    url(r"^$", views.home, name="home"),
    url(r"^trigger", views.trigger_celery, name="trigger_celery"),
    url(r"^404", views.handle_404, name="handle-404"),
]
