import logging
from app import settings
from django.core.management.base import BaseCommand
import os

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Dump current database to diagram image"

    def handle(self, *args, **options):
        # print(f"/Library/PostgreSQL/12/bin/pg_dump --dbname={settings.DATABASE_URL} --schema-only | pg_diagram -o mydb.png -")
        stream = os.popen(f"/Library/PostgreSQL/12/bin/pg_dump --dbname={settings.DATABASE_URL} --schema-only -x")
        output = stream.read()
        output = output.replace("public.", "")
        file_sql_path = os.path.join(settings.BASE_DIR, "logs", 'schema.sql')
        image_sql_path = os.path.join(settings.BASE_DIR, "logs", 'schema.png')
        f = open(file_sql_path, "w")
        f.write(output)
        f.close()

        stream = os.popen(f"pg_diagram --format png -o {image_sql_path} {file_sql_path}")
        output = stream.read()
        print(output)
