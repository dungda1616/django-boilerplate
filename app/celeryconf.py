import os
from celery import Celery
from app import settings

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "app.settings")

app = Celery("app")

CELERY_TIMEZONE = settings.TIME_ZONE

app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks()
